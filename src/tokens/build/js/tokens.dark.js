/**
 * Automatically generated
 * Do not edit directly
 */

export const DATA_VIZ_GREEN_50 = '#133a03';
export const DATA_VIZ_GREEN_100 = '#1a4500';
export const DATA_VIZ_GREEN_200 = '#275600';
export const DATA_VIZ_GREEN_300 = '#366800';
export const DATA_VIZ_GREEN_400 = '#4e7f0e';
export const DATA_VIZ_GREEN_500 = '#619025';
export const DATA_VIZ_GREEN_600 = '#81ac41';
export const DATA_VIZ_GREEN_700 = '#94c25e';
export const DATA_VIZ_GREEN_800 = '#b0d97b';
export const DATA_VIZ_GREEN_900 = '#c6ed94';
export const DATA_VIZ_GREEN_950 = '#ddfab7';
export const DATA_VIZ_AQUA_50 = '#00344b';
export const DATA_VIZ_AQUA_100 = '#004059';
export const DATA_VIZ_AQUA_200 = '#00516c';
export const DATA_VIZ_AQUA_300 = '#006381';
export const DATA_VIZ_AQUA_400 = '#007b9b';
export const DATA_VIZ_AQUA_500 = '#0090b1';
export const DATA_VIZ_AQUA_600 = '#00acc4';
export const DATA_VIZ_AQUA_700 = '#32c5d2';
export const DATA_VIZ_AQUA_800 = '#5edee3';
export const DATA_VIZ_AQUA_900 = '#93f2ef';
export const DATA_VIZ_AQUA_950 = '#b5fefd';
export const DATA_VIZ_BLUE_50 = '#2a2b59';
export const DATA_VIZ_BLUE_100 = '#303470';
export const DATA_VIZ_BLUE_200 = '#374291';
export const DATA_VIZ_BLUE_300 = '#3f51ae';
export const DATA_VIZ_BLUE_400 = '#4e65cd';
export const DATA_VIZ_BLUE_500 = '#617ae2';
export const DATA_VIZ_BLUE_600 = '#7992f5';
export const DATA_VIZ_BLUE_700 = '#97acff';
export const DATA_VIZ_BLUE_800 = '#b7c6ff';
export const DATA_VIZ_BLUE_900 = '#d2dcff';
export const DATA_VIZ_BLUE_950 = '#e9ebff';
export const DATA_VIZ_MAGENTA_50 = '#541d31';
export const DATA_VIZ_MAGENTA_100 = '#661e3a';
export const DATA_VIZ_MAGENTA_200 = '#7c214f';
export const DATA_VIZ_MAGENTA_300 = '#9a2e5d';
export const DATA_VIZ_MAGENTA_400 = '#b93d71';
export const DATA_VIZ_MAGENTA_500 = '#cf4d81';
export const DATA_VIZ_MAGENTA_600 = '#e86e9a';
export const DATA_VIZ_MAGENTA_700 = '#f88aaf';
export const DATA_VIZ_MAGENTA_800 = '#fcacc5';
export const DATA_VIZ_MAGENTA_900 = '#ffccdb';
export const DATA_VIZ_MAGENTA_950 = '#ffe3eb';
export const DATA_VIZ_ORANGE_50 = '#4b2707';
export const DATA_VIZ_ORANGE_100 = '#5e2f05';
export const DATA_VIZ_ORANGE_200 = '#6f3500';
export const DATA_VIZ_ORANGE_300 = '#92430a';
export const DATA_VIZ_ORANGE_400 = '#b14f18';
export const DATA_VIZ_ORANGE_500 = '#c95d2e';
export const DATA_VIZ_ORANGE_600 = '#e07e41';
export const DATA_VIZ_ORANGE_700 = '#e99b60';
export const DATA_VIZ_ORANGE_800 = '#eebd8c';
export const DATA_VIZ_ORANGE_900 = '#f5d6b3';
export const DATA_VIZ_ORANGE_950 = '#fae8d1';
export const THEME_INDIGO_10 = '#14143d';
export const THEME_INDIGO_50 = '#222261';
export const THEME_INDIGO_100 = '#303083';
export const THEME_INDIGO_200 = '#41419f';
export const THEME_INDIGO_300 = '#5252b5';
export const THEME_INDIGO_400 = '#6666c4';
export const THEME_INDIGO_500 = '#8181d7';
export const THEME_INDIGO_600 = '#a2a2e6';
export const THEME_INDIGO_700 = '#c7c7f2';
export const THEME_INDIGO_800 = '#dbdbf8';
export const THEME_INDIGO_900 = '#f1f1ff';
export const THEME_INDIGO_950 = '#f8f8ff';
export const THEME_BLUE_10 = '#04101c';
export const THEME_BLUE_50 = '#0b2640';
export const THEME_BLUE_100 = '#153c63';
export const THEME_BLUE_200 = '#235180';
export const THEME_BLUE_300 = '#346596';
export const THEME_BLUE_400 = '#4977a5';
export const THEME_BLUE_500 = '#628eb9';
export const THEME_BLUE_600 = '#81a5c9';
export const THEME_BLUE_700 = '#a6bdd5';
export const THEME_BLUE_800 = '#b9cadc';
export const THEME_BLUE_900 = '#cdd8e3';
export const THEME_BLUE_950 = '#e6ecf0';
export const THEME_LIGHT_BLUE_10 = '#0a3764';
export const THEME_LIGHT_BLUE_50 = '#0c4277';
export const THEME_LIGHT_BLUE_100 = '#0e4d8d';
export const THEME_LIGHT_BLUE_200 = '#145aa1';
export const THEME_LIGHT_BLUE_300 = '#2268ae';
export const THEME_LIGHT_BLUE_400 = '#3476b9';
export const THEME_LIGHT_BLUE_500 = '#4f8bc7';
export const THEME_LIGHT_BLUE_600 = '#74a3d3';
export const THEME_LIGHT_BLUE_700 = '#a0bedc';
export const THEME_LIGHT_BLUE_800 = '#c1d4e6';
export const THEME_LIGHT_BLUE_900 = '#dde6ee';
export const THEME_LIGHT_BLUE_950 = '#eef3f7';
export const THEME_GREEN_10 = '#052e19';
export const THEME_GREEN_50 = '#0e4328';
export const THEME_GREEN_100 = '#155635';
export const THEME_GREEN_200 = '#1b653f';
export const THEME_GREEN_300 = '#25744c';
export const THEME_GREEN_400 = '#308258';
export const THEME_GREEN_500 = '#499767';
export const THEME_GREEN_600 = '#69af7d';
export const THEME_GREEN_700 = '#8cc497';
export const THEME_GREEN_800 = '#b1d6b5';
export const THEME_GREEN_900 = '#dde9de';
export const THEME_GREEN_950 = '#eef4ef';
export const THEME_RED_10 = '#380700';
export const THEME_RED_50 = '#580d02';
export const THEME_RED_100 = '#761405';
export const THEME_RED_200 = '#8f2110';
export const THEME_RED_300 = '#a13322';
export const THEME_RED_400 = '#ad4a3b';
export const THEME_RED_500 = '#c66e60';
export const THEME_RED_600 = '#d59086';
export const THEME_RED_700 = '#e3bab5';
export const THEME_RED_800 = '#ecd3d0';
export const THEME_RED_900 = '#f4e9e7';
export const THEME_RED_950 = '#faf4f3';
export const THEME_LIGHT_RED_10 = '#5c1105';
export const THEME_LIGHT_RED_50 = '#751709';
export const THEME_LIGHT_RED_100 = '#8b2212';
export const THEME_LIGHT_RED_200 = '#a02e1c';
export const THEME_LIGHT_RED_300 = '#b53a26';
export const THEME_LIGHT_RED_400 = '#c24b38';
export const THEME_LIGHT_RED_500 = '#d36250';
export const THEME_LIGHT_RED_600 = '#e07f6f';
export const THEME_LIGHT_RED_700 = '#ebada2';
export const THEME_LIGHT_RED_800 = '#f6d9d5';
export const THEME_LIGHT_RED_900 = '#faf2f1';
export const THEME_LIGHT_RED_950 = '#fdf9f8';
export const BLACK = '#fff';
export const WHITE = '#333238';
export const BLUE_50 = '#033464';
export const BLUE_100 = '#064787';
export const BLUE_200 = '#0b5cad';
export const BLUE_300 = '#1068bf';
export const BLUE_400 = '#1f75cb';
export const BLUE_500 = '#428fdc';
export const BLUE_600 = '#63a6e9';
export const BLUE_700 = '#9dc7f1';
export const BLUE_800 = '#cbe2f9';
export const BLUE_900 = '#e9f3fc';
export const BLUE_950 = '#f2f9ff';
export const GRAY_10 = '#1f1e24';
export const GRAY_50 = '#333238';
export const GRAY_100 = '#434248';
export const GRAY_200 = '#535158';
export const GRAY_300 = '#626168';
export const GRAY_400 = '#737278';
export const GRAY_500 = '#89888d';
export const GRAY_600 = '#a4a3a8';
export const GRAY_700 = '#bfbfc3';
export const GRAY_800 = '#dcdcde';
export const GRAY_900 = '#ececef';
export const GRAY_950 = '#fbfafd';
export const GREEN_50 = '#0a4020';
export const GREEN_100 = '#0d532a';
export const GREEN_200 = '#24663b';
export const GREEN_300 = '#217645';
export const GREEN_400 = '#108548';
export const GREEN_500 = '#2da160';
export const GREEN_600 = '#52b87a';
export const GREEN_700 = '#91d4a8';
export const GREEN_800 = '#c3e6cd';
export const GREEN_900 = '#ecf4ee';
export const GREEN_950 = '#f1fdf6';
export const ORANGE_50 = '#5c2900';
export const ORANGE_100 = '#703800';
export const ORANGE_200 = '#8f4700';
export const ORANGE_300 = '#9e5400';
export const ORANGE_400 = '#ab6100';
export const ORANGE_500 = '#c17d10';
export const ORANGE_600 = '#d99530';
export const ORANGE_700 = '#e9be74';
export const ORANGE_800 = '#f5d9a8';
export const ORANGE_900 = '#fdf1dd';
export const ORANGE_950 = '#fff4e1';
export const PURPLE_50 = '#232150';
export const PURPLE_100 = '#2f2a6b';
export const PURPLE_200 = '#453894';
export const PURPLE_300 = '#5943b6';
export const PURPLE_400 = '#694cc0';
export const PURPLE_500 = '#7b58cf';
export const PURPLE_600 = '#9475db';
export const PURPLE_700 = '#ac93e6';
export const PURPLE_800 = '#cbbbf2';
export const PURPLE_900 = '#e1d8f9';
export const PURPLE_950 = '#f4f0ff';
export const RED_50 = '#660e00';
export const RED_100 = '#8d1300';
export const RED_200 = '#ae1800';
export const RED_300 = '#c91c00';
export const RED_400 = '#dd2b0e';
export const RED_500 = '#ec5941';
export const RED_600 = '#f57f6c';
export const RED_700 = '#fcb5aa';
export const RED_800 = '#fdd4cd';
export const RED_900 = '#fcf1ef';
export const RED_950 = '#fff4f3';
export const GL_TEXT_PRIMARY = '#ececef'; // Use text.color.default instead
export const GL_TEXT_SECONDARY = '#89888d'; // Use text.color.subtle instead
export const GL_TEXT_TERTIARY = '#737278'; // Use text.color.disabled instead
export const GL_TEXT_COLOR_DEFAULT = '#ececef'; // Used for the default text color.
export const GL_TEXT_COLOR_SUBTLE = '#bfbfc3'; // Used for supplemental text that doesn't need to be as prominent as other text.
export const GL_TEXT_COLOR_STRONG = '#fff'; // Used for text with the highest contrast.
export const GL_TEXT_COLOR_HEADING = '#fff'; // Used for headings level 1-6.
export const GL_TEXT_COLOR_LINK = '#63a6e9'; // Used for default text links.
export const GL_TEXT_COLOR_DISABLED = '#89888d'; // Used for disabled text.
